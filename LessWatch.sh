folder=`pwd`;
while true;
    do
    	N=`find $folder -name "*.less" -not -path "*/boss*"`;
    	fileout=`inotifywait -qe modify $N`;
    	newfileout=${fileout/ MODIFY/};
    	stripped_file=${newfileout/$folder\//}
    	echo -e "\e[90mFound changes in: $stripped_file - compiling...";
	    for f in $newfileout;
	        do
	        	n=${f/css\/less/css}
	        	compile=`lessc --verbose --yui-compress $f ${n%.*}.css`;
	        	if [ "$compile" ]
	        	then
	        		echo -e "\e[32mCompiled\e[0m: $stripped_file -  at `date +"%T"`.";
	        	fi
	    done;
done;