# LessWatch

**This is a constantly developed and updated script and by no means finished!**

### To do:

- Enable passing in of directories to watch and compile to
- Enable passing in of disallowed dirs

## About LessWatch

LessWatch is a command line script which compiles less on save, super compressed.

To get this going you need the following things installed:

- Node
- npm
- less (`npm install less`)
- [inotifytools](https://github.com/rvoicilas/inotify-tools/wiki)

## Usage

Download the LessWatch.sh to somewhere you know (recommend /root). You can symlink this to `/usr/bin` to enable you to just run a command.

e.g.

	ln -s /root/LessWatch.sh /usr/bin/lesswatch
	chmod u+x /usr/bin/lesswatch

From there you can just run `lesswatch` from anywhere on your command line.

Instigate the tool by either running the file directly, or using the symlink and leave it running.

From your location, it will search all sub folders for less files - except ones located in a `boss` folder (this is because we have an [in-house framework](https://github.com/bozboz/boss)).

On save, it will then recompile the minified css with the same name as the less.

**One thing to note: the script expects your folder to be be the following:**

	/css
		style.css
		/less
			style.less
